const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let isConnected;

module.exports = connectToDatabase = () => {
    if (isConnected) {
        console.log('mongo => existing connection');
        return Promise.resolve();
    }

    console.log('mongo => new connection');
    return mongoose.connect(process.env.DB)
        .then(db => {
            isConnected = db.connections[0].readyState;
        });
};